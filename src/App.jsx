import { useEffect, useState } from "react";
import PreviewHtml from "./components/PreviewHtml";

function App() {
  const [htmlContent, setHtmlContent] = useState("");

  const printHtml = (html) => {
    const mywindow = window.open("Print", "", "", "");
    if (mywindow) {
      mywindow.document.write(html);
      mywindow.document.close();
      mywindow.onload = () => {
        mywindow.focus();
        mywindow.print();
        mywindow.close();
      };
    }
  };

  useEffect(() => {
    fetch("/invoice.html")
      .then((response) => response.text())
      .then((data) => setHtmlContent(data))
      .catch((error) => console.error("Error fetching HTML:", error));
  }, []);

  return (
    <>
      <button
        style={{ marginLeft: "10px", marginBottom: "10px" }}
        onClick={() => printHtml(htmlContent)}
      >
        Print
      </button>
      <br />
      <PreviewHtml html={htmlContent} size={"A4"} />
    </>
  );
}

export default App;
