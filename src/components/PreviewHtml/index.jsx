import PropTypes from "prop-types";

const PreviewHtml = (props) => {
  const { html, size, layout, isUseIframe } = props;
  let newContent = html;
  if (isUseIframe && html.indexOf("<html>") === -1) {
    newContent = `<html>
      <head></head>
      <body>${html}</body>
    </html>`;
  }
  return isUseIframe ? (
    <iframe
      title="iframe"
      style={{
        border: "none",
        width: "100%",
        height: "100%",
      }}
      sandbox="allow-scripts allow-same-origin"
      tabIndex="-1"
      srcDoc={newContent}
    />
  ) : (
    <div
      data-size={size}
      data-layout={layout}
      style={{ height: "842px", width: "595px" }}
      // eslint-disable-next-line react/no-danger
      dangerouslySetInnerHTML={{
        __html: html,
      }}
    />
  );
};

PreviewHtml.propTypes = {
  html: PropTypes.string.isRequired,
  size: PropTypes.oneOf(["A4"]),
  layout: PropTypes.oneOf(["portrait"]),
  isUseIframe: PropTypes.bool,
};

PreviewHtml.defaultProps = {
  size: "A4",
  layout: "portrait",
  isUseIframe: false,
};

export default PreviewHtml;
